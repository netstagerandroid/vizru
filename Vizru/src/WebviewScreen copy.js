/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput, Image, Button, Alert, TouchableOpacity,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { WebView } from 'react-native-webview';
import CustomStatusbar from './CustomStatusbar'
const webUrl ='http://projectpreview.org/chatapp/mobile.html';



export default class WebviewScreenCopy extends React.Component { 

  // WebviewScreenCopy({ route, navigation }){
  //   const { id } = route.params.id;
  //   this.state = {temp:id  }
  //   this.setState({temp=id})
  // }
  
  
   render(){ 
    const { route } = this.props;
    //  const {params} = this.props.navigation.;
    const usrag = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0"
 
    console.info ("temp#####")

    const id=route.params.id;
    console.log(route.params.id )
    console.log(webUrl+'?id='+id )


    return (
      <>
      <CustomStatusbar/>
      <View style={{ flex: 1}}>
  
        {/* <WebView source={{html:"<html><body style='color:red'>Hello<br/>This is a test</body></html>"}} style={{width:200,height:200,backgroundColor:'blue',marginTop:20}} /> */}
  
  {/* 
        <Image
          style={{ margin: 6, width: "100%", height: 50, resizeMode: 'contain' }}
          source={require('./src/assets/vizru.png')}
        /> */}
  
        <WebView style={{ flex: 1, }}
       
          // source={{ uri: 'http://projectpreview.org/chatapp/' }}
          source={{uri:webUrl+"?${id}"}}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          startInLoadingState={true}
          scalesPageToFit={true}
          userAgent={usrag}
        />
      </View>
      </>
    );

}

}
// const App: () => React$Node = () => {
  
  // let Splash_Screen = (  
  //      <View style={styles.SplashScreen_RootView}>  
  //          <View style={styles.SplashScreen_ChildView}>  
  //                <Image source={{uri:'https://static.javatpoint.com/tutorial/react-native/images/react-native-tutorial.png'}}  
  //             style={{width:'100%', height: '100%', resizeMode: 'contain'}} />  
  //         </View>  
  //      </View> )  
  // console.log(this.props.navigation.id)
  
// };

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});


