import React, { Component } from 'react';
import { Text, StyleSheet, TextInput, View, Image, Alert } from 'react-native';
import { Button } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import { Container, Header, Content, Form, Item, Input, Label, Icon } from 'native-base';



// npm i react-native-material-textfield for text input
// fadeInUp
// fadeOutDown
// $ npm install react-native-animatable --save


export default class extends Component {
    usernameInput = ref => this.view = ref;
    passwordInput = ref => this.viewpas = ref;

    animUp = () => {
        this.view.animate(Animation = 'fadeIn')
        // this.view.animate(Animation='fadeInUp',)
        this.setState({ placeholder: '' })
    }
    animDown = (selector) => {
        this.view.animate(Animation = 'fadeOut')
        this.setState({ placeholder: 'User Name' })
        // this.view.fadeInUp(500)
    }
    animUppas = () => {
        this.viewpas.animate(Animation = 'fadeIn')
        // this.view.animate(Animation='fadeInUp',)
        this.setState({ placeholderPas: '' })
    }
    animDownpas = (selector) => {
        this.viewpas.animate(Animation = 'fadeOut')
        this.setState({ placeholderPas: 'Password' })
        // this.view.fadeInUp(500)
    }

    static navigationOptions = {
        title: '',
        header: null,
    };
    constructor(props) {
        super(props);
        this.state = { username: "", password: "", placeholder: 'User Name',placeholderPas:'Password' };
    }


    render() {
        const user = require('./assets/user.png')
        return (<View style={styles.main}>
            <Image
                style={styles.logo}
                source={require('./assets/logo.png')}
            />
            {/* <View style={styles.loginSection}> */}

            <Form style={{margin:20}}>
                
            <Item floatingLabel>
            <Icon type='Feather' name="user" style={{color:"#FF6829"}}/>
              <Label>Username</Label>
              
              <Input />
              <Image source={require('./assets/user.png')} style={styles.ImageStyle} />
                
            </Item>
            <Item floatingLabel last>
            <Icon type='Feather' name="lock"  style={{color:"#FF6829"}}/>
              <Label>Password</Label>
              <Input
              onChangeText={(text) => this.setState({ password: text })}
              />
              {/* <Icon type='Feather' name="eye-off"  style={{color:"#FF6829"}}/> */}
            </Item>
            <View style={styles.buttonContainer}>
                        <Button buttonStyle={styles.button} title='Login'
                            onPress={() => onclick(this.state.username, this.state.password, this.props)}
                        />
                    </View>
           
            
          </Form>

                {/* <View style={styles.textIpContaner}>
                    <View>

                        <Animatable.Text ref={this.usernameInput}
                            style={styles.textview}>User Name</Animatable.Text>
                        <View style={styles.SectionStyle}>
                            <Image source={require('./assets/user.png')} style={styles.ImageStyle} />
                            <TextInput
                                style={{ flex: 1 }}
                                onChangeText={(text) => this.setState({ username: text })}
                                value={this.state.username}
                                placeholder={this.state.placeholder}
                                onFocus={this.animUp}
                                onBlur={this.animDown}
                            />
                        </View>

                    </View>
                    <Animatable.Text ref={this.passwordInput} style={styles.textview
                    }>Password</Animatable.Text>
                    <View style={styles.SectionStyle}>
                        <Image source={require('./assets/password.png')} style={styles.ImageStyle} />
                        <TextInput
                            style={{ flex: 1 }}
                            secureTextEntry={true}
                            placeholder={this.state.placeholderPas}
                            onChangeText={(text) => this.setState({ password: text })}
                            onFocus={this.animUppas}
                            onBlur={this.animDownpas}
                        />
                        
                    </View>
                    <View style={styles.buttonContainer}>
                        <Button buttonStyle={styles.button} title='Login'
                            onPress={() => onclick(this.state.username, this.state.password, this.props)}
                        />
                    </View>
                   
                </View> */}
            {/* </View> */}
        </View>)
    }

}

function onclick(user, pass, props) {
    console.log(user + "  " + pass)
    props.navigation.navigate('WebViewScreen', { user, pass });
    // this.props.navigation.navigate('WebViewScreen',{user,pass})
}
function loginApiCall() {

}

const styles = StyleSheet.create({
    loginSection: {
        backgroundColor: 'white',
        flex: 1,
    },
    main: {
        backgroundColor: 'white',
        flex: 1,
    },
    textIpContaner: {
        justifyContent: "center",
        alignContent: "center",
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    textipBox: {
        borderBottomWidth: 1,
        height: 40,
        borderColor: 'black',
    },
    textview: {
        color: "gray",
        top: 0,
        left: 0,
        marginTop: 30,
        marginLeft: 35,
    },
    logo: {
        marginTop: 90,
        marginBottom: 90,
        margin: 6,
        width: "60%",
        height: 50,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    buttonContainer: {
        marginTop: 50,
    },
    button: {
        backgroundColor: '#FF6829',
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 8,
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderColor: '#000',
        height: 40,
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
})