import React, { Component } from 'react';
import { Text, StyleSheet, TextInput, View, Image, ActivityIndicator, Alert, ToastAndroid, } from 'react-native';
import { Button } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import base64 from 'react-native-base64';

// npm i react-native-material-textfield for text input
// fadeInUp
// fadeOutDown
// $ npm install react-native-animatable --save
// let base64 = require('base-64');
const loginUrl = 'https://home.vizru.com/sys/api.v1';

export default class extends Component {
    usernameInput = ref => this.view = ref;
    passwordInput = ref => this.viewpas = ref;

    animUp = () => {
        if(this.state.username.length>0){
            //   do nothing
            }else{
        this.setState({ placeholder: '', usernameHidden: 'User Name' })
        // this.view.animate(Animation = 'fadeIn', duration = 500)
        // this.view.animate(Animation='fadeInUp',)   
        this.view.fadeIn(500);
            }
    }
    animDown = (selector) => {
        if(this.state.username.length>0){
        //   do nothing
        }else{
            // this.view.animate(Animation = 'fadeOut', duration = 500)
              this.view.fadeOut(500)
            this.setState({ placeholder: 'User Name', usernameHidden: '' })
        }
       
        
      
    }
    animUppas = () => {
        if(this.state.password.length>0){
            //   do nothing
            }else{
        // this.viewpas.animate(Animation = 'fadeIn', duration = 500)
        this.viewpas.fadeIn(500)
        // this.view.animate(Animation='fadeInUp',)
        this.setState({ placeholderPas: '', passwordHidden: 'Password' })
            }
    }
    animDownpas = (selector) => {
        if(this.state.password.length>0){
            //   do nothing
            }else{
        // this.viewpas.animate(Animation = 'fadeOut', duration = 500)
        this.viewpas.fadeOut(500)
        this.setState({ placeholderPas: 'Password', passwordHidden: '' })
        // this.view.fadeInUp(500)  
            }


    }

    static navigationOptions = {
        title: '',
        header: null,
    };
    constructor(props) {
        super(props);
        this.state = {
            username: "", password: "",
            placeholder: 'User Name', placeholderPas: 'Password',
            usernameHidden: '',
            passwordHidden: '',
            isloading: false,
        };
    }



    render() {
        const user = require('./assets/user.png')
        return (<View style={styles.main}>
              <ActivityIndicator style={{ left: 0, right: 0, top: 0, bottom: 0 }} animating={this.state.isloading} size="large" color="#0000ff" />

            <Image
                style={styles.logo}
                source={require('./assets/logo.png')}
            />
          
            <View style={styles.loginSection}>
                <View style={styles.textIpContaner}>
                    <View>
                        
                        <Animatable.Text ref={this.usernameInput}
                            style={styles.textview}>{this.state.usernameHidden}</Animatable.Text>
                        <View style={styles.SectionStyle}>
                            <Image source={require('./assets/user.png')} style={styles.ImageStyle} />
                            <TextInput
                                style={{ flex: 1 }}
                                onChangeText={(text) => this.setState({ username: text })}
                                value={this.state.username}
                                placeholder={this.state.placeholder}
                                onFocus={this.animUp}
                                onBlur={this.animDown}
                            />
                        </View>
                    </View>
                    <Animatable.Text ref={this.passwordInput} style={styles.textview
                    }>{this.state.passwordHidden}</Animatable.Text>
                    <View style={styles.SectionStyle}>
                        <Image source={require('./assets/password.png')} style={styles.ImageStyle} />
                        <TextInput
                            style={{ flex: 1 }}
                            secureTextEntry={true}
                            placeholder={this.state.placeholderPas}
                            onChangeText={(text) => this.setState({ password: text })}
                            onFocus={this.animUppas}
                            onBlur={this.animDownpas}
                            value={this.state.password}
                        />
                    </View>
                    <View style={styles.buttonContainer}>
                        <Button buttonStyle={styles.button} title='Login'
                            onPress={() =>this.setState({ isloading: true }, () => { onclick(this.state.username, this.state.password, this.props,this) })
                                }
                        />
                    </View>
                </View>
            </View>
        </View>)
    }
}

function onclick(user, pass, props,context){

    let reg = /\S+@\S+.\S+/;
   
    if(user==''){
        ToastAndroid.show('Enter Username', ToastAndroid.SHORT);
        context.setState({isloading:false})
       
    }else if(pass==''){
        ToastAndroid.show('Enter Password', ToastAndroid.SHORT);
    }else if(reg.test(String(user).toLowerCase())===false)
    {
        ToastAndroid.show('Enter valid email', ToastAndroid.SHORT);
    }else if(!pass.length>5)
    {
        ToastAndroid.show('Enter valid password', ToastAndroid.SHORT);
    }else{
        console.log(user + "  " + pass)
        loginApiCall(user, pass,props,context);
    }
    
   
    // loginApiCall('ashik@vizru.com', 'asd123',props);

  
    // this.props.navigation.navigate('WebViewScreen',{user,pass})
}
function loginApiCall(user, pass,props,context) {
   
    var headers = new Headers();
    console.log(user + " :" + pass)
    headers.append('Authorization', 'Basic ' + base64.encode(user + ":" + pass));
    headers.append('contentType', "application/x-www-form-urlencoded; charset=utf-8");
    headers.append('accept', 'application/json, text/javascript, /; q=0.01');
    headers.append('accept-language', 'en-US,en;q=0.9,ml;q=0.8,es;q=0.7');
    headers.append('referrerPolicy', 'no-referrer-when-downgrade');
    let formdata = new FormData();
    formdata.append('op', 'user.authenticate')
    

    return fetch(loginUrl, {
    // return fetch('https://home.vizru.com/sys/api.v1', {
        method: 'POST',
        headers: headers,
        body: formdata,
    }
    )
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('##################################')
            console.log(JSON.stringify(responseJson))
            console.log('##################################')
            console.log(responseJson)
            // console.log(responseJson.Status[0])
            // console.log(responseJson.Body.Id)
            // console.log(responseJson.Errors[0])
            
            // ToastAndroid.show('Oops! Something went wrong', ToastAndroid.SHORT);
            context.setState({isloading:false});
          if(responseJson.Status[0]==200){
            props.navigation.replace('WebviewScreenCopy', { id:responseJson.Body.Id });
          }else{
            
        ToastAndroid.show(responseJson.Errors[0], ToastAndroid.SHORT);
          }
            // if(responseJson.Status[1]==200){
                
            // }
            // responseJson.
            // console.log(responseJson.status)
            // this.setState({
            //     //   isLoading: false,
            //     //   dataSource: responseJson.movies,
            // }, function () {
            //     console.log(responseJson)
            // });

        })
        .catch((error) => {
            console.error(error);
            console.log("ERR ########" + error);
            ToastAndroid.show('Oops! Something went wrong', ToastAndroid.SHORT);

        }).done();
}

const styles = StyleSheet.create({
    loginSection: {
        backgroundColor: 'white',
        flex: 1,
    },
    main: {
        backgroundColor: 'white',
        flex: 1,
    },
    textIpContaner: {
        justifyContent: "center",
        alignContent: "center",
        marginTop: 0,
        marginLeft: 20,
        marginRight: 20,
    },
    textipBox: {
        borderBottomWidth: 1,
        height: 40,
        borderColor: 'black',
    },
    textview: {
        color: "gray",
        top: 0,
        left: 0,
        marginTop: 0,
        marginLeft: 35,
    },
    logo: {
        marginTop: 90,
        marginBottom: 90,
        margin: 6,
        width: "60%",
        height: 50,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    buttonContainer: {
        marginTop: 50,
    },
    button: {
        backgroundColor: '#FF6829',
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 8,
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderColor: '#000',
        height: 40,
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
})